package mshort.finalproject;

// Temperature Client, written by Michael Short (CIT 360 BYU-I)
//

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class TempClient {

    static String serverAddress = "http://localhost:8080";
    static char input = 0;
    static ConversionRequest currentRequest = null;
    static List<ConversionRequest> myConversions = null;

    public static void main(String[] args) {
        do {
            int invalidSelections = 0;

            printTitle();

            String jsonConversionsFromServer = clientCommunication.getConversionsFromServer(serverAddress);
            if (jsonConversionsFromServer.equals("ERROR")) {
                System.out.println("ERROR: Unable to Contact Server, Please Verify Server Availability and Try Again.");
                System.out.println("Exiting...");
                System.exit(1);
            } else {
                myConversions = jsonConversion.jsonToConversionRequestList(jsonConversionsFromServer);
                int myConversionsSize = myConversions.size();
                if (myConversionsSize > 0) {
                    System.out.println("Last Conversions Performed");
                    System.out.println("--------------------------");
                    int currentItem = 0;
                    for (ConversionRequest request : myConversions) {
                        String lineOutput = request.getInValue() + " " + request.getInScaleType() + "\t=\t " +
                                request.getOutValue() + " " + request.getOutScaleType();
                        if (currentItem == 0) {
                            // Highlight the answer to last conversion made so it stands out
                            System.out.println(" [ " + lineOutput + " ] ");
                        } else {
                            System.out.println("   " + lineOutput);
                        }
                        currentItem++;
                    }
                    System.out.println();
                }

            }

            // Menu System
            input = 0;
            displayMenu();
            while (!validSelection(input)) {
                input = getMenuChoice();
                if (validSelection(input)) {
                    break;
                } else {
                    System.out.println("Invalid Selection!");
                    invalidSelections++;
                    if (invalidSelections >= 5) {
                        break;
                    }
                }
            }

            if (invalidSelections >= 5) {
                System.out.println("Starting Over!");
                continue;
            } else {
                // Process the selection
                if (!validSelection(input)) {
                    // This would be an assertion
                    System.out.println("ERROR! We should never get here and have an invalid input");
                }

                // If the selection was to quit, continue and the outer loop will catch that we are quitting
                if (input == 'q' || input == 'Q') {
                    continue;
                } else {
                    // Process the Valid Selections Here (F to C or C to F currently)
                    // Logic would need to be changed if we added a third scale apart from C or F.
                    currentRequest = new ConversionRequest();
                    if (input == 'f' || input == 'F') {
                        currentRequest.setInValue(getTemperature('F'));
                        currentRequest.setInScaleType("F");
                        currentRequest.setOutScaleType("C");
                        //System.out.println(jsonConversion.conversionRequestToJson(currentRequest));
                        String srvResponse = clientCommunication.sendConversionToServer(
                                serverAddress,
                                jsonConversion.conversionRequestToJson(currentRequest));
                        System.out.println("Server Responded: " + srvResponse);
                    }
                    if (input == 'c' || input == 'C') {
                        currentRequest.setInValue(getTemperature('C'));
                        currentRequest.setInScaleType("C");
                        currentRequest.setOutScaleType("F");
                        //System.out.println(jsonConversion.conversionRequestToJson(currentRequest));
                        String srvResponse = clientCommunication.sendConversionToServer(
                                serverAddress,
                                jsonConversion.conversionRequestToJson(currentRequest));
                        System.out.println("Server Responded: " + srvResponse);
                    }
                    // Could be Expanded Later for Kelvin or other Measurement Scale
                }
            }
        } while (input != 'q' && input != 'Q');

        System.out.println();
        System.out.println("Thank you for using Temperature Client");
        System.out.println("Exiting...");
    }

    static boolean validSelection(char inChar) {
        boolean result = false;
        if (input == 'c' || input == 'C' ||
                input == 'f' || input == 'F' ||
                input == 'q' || input == 'Q') {
            result = true;
        }
        return result;
    }

    static void printTitle() {
        System.out.println("Temperature Client (by Michael Short)");
        System.out.println();
    }

    static void displayMenu() {
        System.out.println("Please Select Desired Operation:");
        System.out.println();
        System.out.println("[F] Convert F to C");
        System.out.println("[C] Convert C to F");
        System.out.println("[Q] Quit");
        System.out.println();
    }

    static char getMenuChoice() {
        char result = 0;
        System.out.print("Choice: ");
        try {
            result = (char) System.in.read();
            // this is to flush all other characters entered so they do not buffer in System.in
            while(System.in.available() > 0) {
                System.in.read();
            }
        } catch (IOException ex) {
            System.out.println("IOException Encountered: " + ex.getMessage());
        }
        return result;
    }

    static double getTemperature(char scale) {
        double result = 0;
        boolean valid = false;
        while (!valid) {
            String temperatureString = "";
            Scanner myScanner = new Scanner(System.in);
            System.out.print("Please enter Temperature in (" + scale + "): ");
            temperatureString = myScanner.next();
            // Intent here is to take a pos or neg value, that can contain numbers after and a decimal point
            // We then count the decimal points after to make sure we have 1 or less. More than one decimal
            // point would be a failure. 1 or less decimal points after passing the regex match is valid.
            if (temperatureString.matches("^-?[0-9.]+$")) {
                int count = 0;
                for (int x = 0; x < temperatureString.length(); x++) {
                    if (temperatureString.charAt(x) == '.') {
                        count++;
                    }
                }
                if (count <= 1) {
                    valid = true;
                }
            }
            if (!valid) {
                System.out.println("Invalid Selection!");
            } else {
                result = Double.parseDouble(temperatureString);
            }
        }
        return result;
    }

}
