package mshort.finalproject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.*;


/* AccessTemperatureDB implemented using a singleton
   Used to get customer data from my MYSQL database */
public class AccessTemperatureDB {

    SessionFactory factory;
    Session session;

    private static AccessTemperatureDB single_instance = null;

    private AccessTemperatureDB()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /* Establish singleton model. This maintains only one instance of the
       class, creating it the first time if one does not exists */
    public static AccessTemperatureDB getInstance()
    {
        if (single_instance == null) {
            single_instance = new AccessTemperatureDB();
        }

        return single_instance;
    }

    // This Will get the last 10 conversions performed from the database
    public List<ConversionRequest> getConversions() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from mshort.finalproject.ConversionRequest ORDER BY id DESC";
            Query myQuery = session.createQuery(sql);
            myQuery.setFirstResult(0);
            myQuery.setMaxResults(10);
            List<ConversionRequest> ucs = (List<ConversionRequest>) myQuery.getResultList();
            //List<ConversionRequest> ucs = (List<ConversionRequest>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return ucs;

        } catch (Exception e) {
            System.out.println("Exception encountered in: getUsedCars");
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    /* Used to insert a single ConversionRequest into database */
    public void addConversionRequest(ConversionRequest inConversionRequest) {

        try {
            session = factory.openSession();
            Transaction t = session.getTransaction();
            session.getTransaction().begin();
            session.saveOrUpdate(inConversionRequest);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Exception encountered in: addConversionRequest");
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

}