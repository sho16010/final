package mshort.finalproject;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.List;

import com.sun.net.httpserver.*;

public class TempServer {

    static String responseSuccess = "SUCCESS";
    static String responseFail = "FAIL";

    public static void main(String[] args) {
        TempServer myHttpServer = new TempServer();
        try {
            myHttpServer.startServer();
        } catch (Exception e){
            System.out.println("Exception Encountered in main: " + e.getMessage());
        }
    }

    private void startServer() throws Exception {
        HttpServer myServer = HttpServer.create(new InetSocketAddress(8080), 0);
        myServer.createContext("/", new MyHttpHandler());
        myServer.setExecutor(null);
        myServer.start();
    }

    class MyHttpHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange he) throws IOException {
            String serverResponse = "";
            String method = he.getRequestMethod();
            System.out.println("Method: " + method);
            if (method.equals("GET")) {
                List<ConversionRequest> myConversions = AccessTemperatureDB.getInstance().getConversions();
                serverResponse = jsonConversion.conversionRequestListToJson(myConversions);
                //System.out.println("serverResponse: " + serverResponse);
            } else if (method.equals("POST")) {
                boolean accepted = true;
                InputStream myInputStream = he.getRequestBody();
                int incomingBytes = myInputStream.available();
                if (incomingBytes <= 0) {
                    accepted = false;
                    myInputStream.close();
                } else {
                    // How many bytes of data came in? Allocate space for them and read them into buffer
                    byte[] myBytes = new byte[incomingBytes];
                    int bytesRead = myInputStream.read(myBytes, 0, incomingBytes);
                    myInputStream.close();
                    if (bytesRead <= 0) {
                        accepted = false;
                        System.out.println("Error: HttpHandler read from InputStream failed!");
                    } else {
                        String inJsonRequest = new String(myBytes, StandardCharsets.UTF_8);
                        System.out.println("inJsonRequest: " + inJsonRequest);
                        if (jsonConversion.validJsonRequest(inJsonRequest)) {
                            ConversionRequest incomingRequest = jsonConversion.jsonToConversionRequest(inJsonRequest);
                            //System.out.println("ConversionRequest object: " + incomingRequest.toString());

                            // Perform Conversion and fill in the OutValue in the object, then write to DB
                            if (conversionIsValid(incomingRequest)) {
                                incomingRequest.setOutValue(CalculateTemperature(incomingRequest));
                                AccessTemperatureDB.getInstance().addConversionRequest(incomingRequest);
                            } else {
                                // If conversion is not valid then reject it
                                System.out.println("Error: incoming ConversionRequest failed validation check");
                                accepted = false;
                            }
                        } else {
                            accepted = false;
                            System.out.println("Error: JSON from Client Was Not Valid or did not contain a ConversionRequest object");
                        }
                    }
                }
                if (accepted) {
                    serverResponse = responseSuccess;
                } else {
                    serverResponse = responseFail;
                }
            } else {
                System.out.println("ERROR HttpHandler does not have implementation for method: " + method);
                serverResponse = "ERROR HttpHandler does not have implementation for method: " + method;
            }
            he.sendResponseHeaders(200, serverResponse.length());
            OutputStream myOutputStream = he.getResponseBody();
            myOutputStream.write(serverResponse.getBytes(StandardCharsets.UTF_8));
            myOutputStream.flush();
            myOutputStream.close();

            System.out.println("HttpJsonServer: Handled incoming HTTP Connection, sent following info to client:\n" + serverResponse);
        }
    }

    public static double CalculateTemperature(ConversionRequest inConversion) {
        double result = 0;

        // Convert F to C and C to F using conversion methods that clip to 2 decimal places
        if (inConversion.getInScaleType().equals("F") && inConversion.getOutScaleType().equals("C")) {
            result = TemperatureConversion.convertFtoC2DecimalPlaces(inConversion.getInValue());
        } else if (inConversion.getInScaleType().equals("C") && inConversion.getOutScaleType().equals("F")) {
            result = TemperatureConversion.convertCtoF2DecimalPlaces(inConversion.getInValue());
        }

        return result;
    }

    public static boolean conversionIsValid(ConversionRequest inConversion) {
        boolean result = false;

        if ((inConversion.getInScaleType().equals("F") && inConversion.getOutScaleType().equals("C")) ||
            (inConversion.getInScaleType().equals("C") && inConversion.getOutScaleType().equals("F"))) {
            result = true;
        } else {
            System.out.println("ERROR: Request Made to Convert Unsupported Type!");
            System.out.println("ERROR:  IN Type Requested [" + inConversion.getInScaleType() + "]");
            System.out.println("ERROR: OUT Type Requested [" + inConversion.getOutScaleType() + "]");
        }

        return result;
    }

}


