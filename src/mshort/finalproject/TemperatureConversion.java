package mshort.finalproject;

public class TemperatureConversion {

    public static double convertFtoC(double inTempF) {
        double result = 0.0;

        try {
            result = (inTempF - 32) * 5 / 9;
        } catch (Exception e) {
            System.out.println("Exception encountered in convertFtoC:");
            System.out.println("Exception String: " + e.toString());
            System.out.println("Exception Message: " + e.getMessage());
            System.out.println("Verify inTempF Parameter Value");
            throw e;
        }

        return result;
    }

    public static double convertCtoF(double inTempC) {
        double result = 0.0;

        try {
            result = ((inTempC * 9) / 5) + 32;
        } catch (Exception e) {
            System.out.println("Exception encountered in convertCtoF:");
            System.out.println("Exception String: " + e.toString());
            System.out.println("Exception Message: " + e.getMessage());
            System.out.println("Verify inTempC Parameter Value");
            throw e;
        }

        return result;
    }

    public static double convertCtoF2DecimalPlaces(double inTempC) {
        double result = 0.0;

        try {
            result = convertCtoF(inTempC);
            result = Math.round(result * 100.0) / 100.0;
        } catch (Exception e) {
            System.out.println("Exception encountered in convertCtoF2DecimalPlaces:");
            System.out.println("Exception Message: " + e.getMessage());
        }

        return result;
    }

    public static double convertFtoC2DecimalPlaces(double inTempF) {
        double result = 0.0;

        try {
            result = convertFtoC(inTempF);
            result = Math.round(result * 100.0) / 100.0;
        } catch (Exception e) {
            System.out.println("Exception encountered in convertFtoC2DecimalPlaces:");
            System.out.println("Exception Message: " + e.getMessage());
        }

        return result;
    }

}
