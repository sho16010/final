package mshort.finalproject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class clientCommunication {

    public static String getConversionsFromServer(String address) {
        String result = "";

        try {
            URL myUrl = new URL(address);
            HttpURLConnection myConnection = (HttpURLConnection) myUrl.openConnection();
            BufferedReader myReader = new BufferedReader(new InputStreamReader(myConnection.getInputStream()));
            StringBuilder myStringBuilder = new StringBuilder();
            String currentLine = null;
            while ((currentLine = myReader.readLine()) != null) {
                myStringBuilder.append(currentLine + '\n');
            }
            result = myStringBuilder.toString();
        } catch (Exception x) {
            result = "ERROR";
            System.out.println("Exception Caught in getConversionsFromServer: " + x.getMessage() + "[" + x.toString() + "]");
        }

        return result;
    }

    public static String sendConversionToServer(String address, String outgoingJson) {
        String result = "";

        try {
            URL myUrl = new URL(address);
            HttpURLConnection myConnection = (HttpURLConnection) myUrl.openConnection();
            myConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            myConnection.setRequestMethod("POST");
            myConnection.setDoOutput(true);
            myConnection.setDoInput(true);

            OutputStream myOutputStream = myConnection.getOutputStream();
            myOutputStream.write(outgoingJson.getBytes(StandardCharsets.UTF_8));
            myOutputStream.close();

            BufferedReader myReader = new BufferedReader(new InputStreamReader(myConnection.getInputStream()));
            StringBuilder myStringBuilder = new StringBuilder();
            String currentLine = null;
            while ((currentLine = myReader.readLine()) != null) {
                myStringBuilder.append(currentLine + '\n');
            }
            result = myStringBuilder.toString();
        } catch (Exception x) {
            System.out.println("Exception Caught in sendConversionToServer: " + x.getMessage() + "[" + x.toString() + "]");
        }

        return result;
    }


}
