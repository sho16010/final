package mshort.finalproject;

import javax.persistence.*;

@Entity
@Table(name = "conversions")
public class ConversionRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "inValue")
    private double inValue;

    @Column(name = "outValue")
    private double outValue;

    @Column(name = "inScale")
    private String inScaleType;

    @Column(name = "outScale")
    private String outScaleType;

    public void setInValue(double inVal) {
        this.inValue = inVal;
    }

    public double getInValue() {
        return this.inValue;
    }

    public void setOutValue(double outVal) {
        this.outValue = outVal;
    }

    public double getOutValue() {
        return this.outValue;
    }

    public void setInScaleType(String inScaleVal) {
        this.inScaleType = inScaleVal;
    }

    public String getInScaleType() {
        return this.inScaleType;
    }

    public void setOutScaleType(String outScaleVal) {
        this.outScaleType = outScaleVal;
    }

    public String getOutScaleType() {
        return this.outScaleType;
    }

    public String toString() {
        return "inValue: " + inValue + ", outValue: " + outValue + ", inScaleType: " + inScaleType + ", outScaleType: " + outScaleType;
    }

}
