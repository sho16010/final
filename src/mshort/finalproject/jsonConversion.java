package mshort.finalproject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

public class jsonConversion {

    public static ConversionRequest jsonToConversionRequest(String inJsonString) {
        ConversionRequest myConversionRequest = null;
        ObjectMapper myMapper = new ObjectMapper();

        try {
            myConversionRequest = myMapper.readValue(inJsonString, ConversionRequest.class);
        } catch (JsonProcessingException x) {
            System.out.println("JSON Processing Exception Encountered: " + x.getMessage());
        } catch (Exception e) {
            System.out.println("Exception Encountered: " + e.getMessage());
        }

        return myConversionRequest;
    }

    public static String conversionRequestToJson(ConversionRequest inConversionRequest) {
        String result = "";
        ObjectMapper myMapper = new ObjectMapper();

        try {
            result = myMapper.writeValueAsString(inConversionRequest);
        } catch (JsonProcessingException x) {
            System.out.println("JSON Processing Exception Encountered: " + x.getMessage());
        } catch (Exception e) {
            System.out.println("Exception Encountered: " + e.getMessage());
        }

        return result;
    }

    public static String conversionRequestListToJson(List<ConversionRequest> inConversionRequests) {
        String result = "";
        ObjectMapper myMapper = new ObjectMapper();

        try {
            result = myMapper.writeValueAsString(inConversionRequests);
        } catch (JsonProcessingException x) {
            System.out.println("JSON Processing Exception Encountered: " + x.getMessage());
        } catch (Exception e) {
            System.out.println("Exception Encountered: " + e.getMessage());
        }

        return result;
    }

    public static List<ConversionRequest> jsonToConversionRequestList(String inJsonString) {
        List<ConversionRequest> myConversionRequests = null;
        ObjectMapper myMapper = new ObjectMapper();

        try {
            myConversionRequests = myMapper.readValue(inJsonString, new TypeReference<List<ConversionRequest>>(){});
        } catch (JsonProcessingException x) {
            System.out.println("JSON Processing Exception Encountered: " + x.getMessage());
        } catch (Exception e) {
            System.out.println("Exception Encountered: " + e.getMessage());
        }

        return myConversionRequests;
    }

    public static boolean validJsonRequest(String inJsonRequest) {
        // This is an attempt to best verify that the JSON data from the Client is Valid
        // and contains the fields that describe the type of object we will be converting
        boolean result = false;

        try {
            // Part One - Is This Json Data We Can Read and Map?
            ObjectMapper myMapper = new ObjectMapper();
            myMapper.readTree(inJsonRequest);

            // Part Two - Does it Contain the Strings of the Values for our Object?
            if (inJsonRequest.contains("inValue") &&
                    inJsonRequest.contains("outValue") &&
                    inJsonRequest.contains("inScaleType") &&
                    inJsonRequest.contains("outScaleType")) {
                result = true;
            }
        } catch (JsonMappingException me) {
            return false;
        } catch (JsonProcessingException pe) {
            return false;
        } catch (Exception e) {
            return false;
        }

        return result;
    }

}
